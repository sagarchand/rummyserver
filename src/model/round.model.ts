import { PlayerScore } from './playerscore.model';

export class Round {
	id: number;
	scores: PlayerScore[];
};