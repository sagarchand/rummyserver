export class PlayerScore {
	playerName: String;
	playerScore: number;

	constructor() {
		this.playerName = '';
		this.playerScore = 0;
	}
}