import { Player } from './player.model';
import { Settings } from './settings.model';
import { Round } from './round.model';

export class GameRoom {
 id: number = 0;
 roomName: string;
 players: Player[];
 rounds: Round[];
 roundCount: number;
 settings: Settings;

 constructor() {
 	this.id = this.id + 1;
 	this.roomName = "";
 	this.players = [];
 	this.rounds = [];
 	this.roundCount = 1;
 	this.settings =  new Settings();
 }

}